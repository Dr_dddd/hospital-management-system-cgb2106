import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../components/Login.vue'
import ElementUI from '../components/ElementUI.vue'
import Home from '../components/Home.vue'
import User from '../components/user/user.vue'
import Room from '../components/hq/room.vue'
import Bed from '../components/hq/bed.vue'
import Patient from '../components/patient/Patient.vue'
import Potion from '../components/potion/potion.vue'
import Dept from '../components/dept/Dept.vue'
import Personnel from '../components/personnel/Personnel.vue'
import Instrument from '../components/instrument/instrument.vue'
import Update from '../components/Update.vue'
//使用路由机制
Vue.use(VueRouter)
const routes = [
  {path: '/', redirect: '/login'},
  {path: '/login', component: Login},
  {path: '/elementUI', component: ElementUI},
  {path: '/update', component: Update},
  {path: '/home', component: Home ,children :[
      {path: '/user',component: User},      //韩云
      {path:'/patient',component: Patient},   //冯浩天
      {path: '/room',component: Room},      //雷世军
      {path: '/bed',component: Bed},     //雷世军
      {path: '/potion',component:Potion},  //竹肖臣
      {path: '/dept',component:Dept},		//冯晶涛
      {path: '/personnel',component:Personnel},  //冯晶涛
      {path: '/instrument',component:Instrument}   //李文斌
    ],
  },
]

//路由导航守卫!!!!!!!

const router = new VueRouter({
  routes
})

/* 定义路由导航守卫
 参数一: to 路由跳转的网址
 参数二: from 路由从哪里来
 参数三next 是一个函数,表示放行或者重定向
      next() next("/login")重定向
业务实现:
  核心逻辑: 检查是否有token
    有token 表示已经登录,放行请求
    没有token 表示用户没有登陆,重定向值登录页面
    如果访问到login页面,直接放行
*/
router.beforeEach((to,from,next) => {
  if(to.path === "/login")
    return next()
  let token = window.sessionStorage.getItem("token")
  if (token){
    return next()
  }
  next("/login")



})

export default router
