/*
SQLyog 企业版 - MySQL GUI v8.14 
MySQL - 5.5.5-10.3.7-MariaDB 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

create table `potion` (
	`potion_id` double ,
	`potion_no` varchar (60),
	`potion_name` varchar (90),
	`potion_price` double ,
	`potion_quantity` double ,
	`potion_status` tinyint (1),
	`potion_ren` varchar (90),
	`created` datetime ,
	`updated` datetime 
); 
insert into `potion` (`potion_id`, `potion_no`, `potion_name`, `potion_price`, `potion_quantity`, `potion_status`, `potion_ren`, `created`, `updated`) values('2','yw002','青霉素','120','5000','1','抗生素','2021-03-22 21:25:06','2021-09-29 19:15:19');
insert into `potion` (`potion_id`, `potion_no`, `potion_name`, `potion_price`, `potion_quantity`, `potion_status`, `potion_ren`, `created`, `updated`) values('3','yw003','胰岛素','16','500','1','糖尿病','2021-03-22 21:25:06','2021-03-22 21:25:06');
insert into `potion` (`potion_id`, `potion_no`, `potion_name`, `potion_price`, `potion_quantity`, `potion_status`, `potion_ren`, `created`, `updated`) values('4','yw004','阿莫西林','30','500','1','抗生素','2021-03-22 21:25:06','2021-03-22 21:25:06');
insert into `potion` (`potion_id`, `potion_no`, `potion_name`, `potion_price`, `potion_quantity`, `potion_status`, `potion_ren`, `created`, `updated`) values('5','yw005','碘伏','5','500','1','消炎','2021-03-22 21:25:06','2021-03-22 21:25:06');
insert into `potion` (`potion_id`, `potion_no`, `potion_name`, `potion_price`, `potion_quantity`, `potion_status`, `potion_ren`, `created`, `updated`) values('6','yw006','头孢','20','540','1','抗生素','2021-03-22 21:25:06','2021-03-22 21:25:06');
insert into `potion` (`potion_id`, `potion_no`, `potion_name`, `potion_price`, `potion_quantity`, `potion_status`, `potion_ren`, `created`, `updated`) values('7','yw007','青霉素','15','500','1','抗生素','2021-03-22 21:25:06','2021-03-22 21:25:06');
insert into `potion` (`potion_id`, `potion_no`, `potion_name`, `potion_price`, `potion_quantity`, `potion_status`, `potion_ren`, `created`, `updated`) values('8','yw008','胰岛素','16','500','1','糖尿病','2021-03-22 21:25:06','2021-03-22 21:25:06');
insert into `potion` (`potion_id`, `potion_no`, `potion_name`, `potion_price`, `potion_quantity`, `potion_status`, `potion_ren`, `created`, `updated`) values('9','yw009','阿莫西林','30','500','1','抗生素','2021-03-22 21:25:06','2021-03-22 21:25:06');
insert into `potion` (`potion_id`, `potion_no`, `potion_name`, `potion_price`, `potion_quantity`, `potion_status`, `potion_ren`, `created`, `updated`) values('10','yw010','碘伏','5','500','1','消炎','2021-03-22 21:25:06','2021-03-22 21:25:06');
insert into `potion` (`potion_id`, `potion_no`, `potion_name`, `potion_price`, `potion_quantity`, `potion_status`, `potion_ren`, `created`, `updated`) values('11','yw011','甲硝唑凝胶','12','300','1','外用药','2021-09-29 19:18:05','2021-09-29 19:18:05');
