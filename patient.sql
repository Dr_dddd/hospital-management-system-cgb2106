/*
SQLyog 企业版 - MySQL GUI v8.14 
MySQL - 5.5.5-10.3.7-MariaDB 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

create table `patient` (
	`id` double ,
	`patient_name` varchar (240),
	`sex` varchar (30),
	`state` varchar (120),
	`room_id` varchar (30),
	`bed_id` varchar (30),
	`status` tinyint (1),
	`created` datetime ,
	`updated` datetime 
); 
insert into `patient` (`id`, `patient_name`, `sex`, `state`, `room_id`, `bed_id`, `status`, `created`, `updated`) values('2','101-2','男','2222','fj101','bc002','1','2021-10-12 13:54:22','2021-10-12 13:54:22');
insert into `patient` (`id`, `patient_name`, `sex`, `state`, `room_id`, `bed_id`, `status`, `created`, `updated`) values('3','101-3','男','3333','fj101','bc003','1','2021-10-12 13:54:39','2021-10-12 13:54:39');
insert into `patient` (`id`, `patient_name`, `sex`, `state`, `room_id`, `bed_id`, `status`, `created`, `updated`) values('4','101-4','男','4444','fj101','bc004','1','2021-10-12 13:54:52','2021-10-12 13:54:52');
insert into `patient` (`id`, `patient_name`, `sex`, `state`, `room_id`, `bed_id`, `status`, `created`, `updated`) values('5','101-5','女','5555','fj101','bc005','1','2021-10-12 13:55:08','2021-10-12 14:39:58');
insert into `patient` (`id`, `patient_name`, `sex`, `state`, `room_id`, `bed_id`, `status`, `created`, `updated`) values('6','101-6','女','6666','fj101','bc006','1','2021-10-12 13:55:30','2021-10-12 16:12:47');
