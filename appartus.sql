/*
SQLyog 企业版 - MySQL GUI v8.14 
MySQL - 5.5.5-10.3.7-MariaDB 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

create table `appartus` (
	`appartus_id` double ,
	`appartus_name` varchar (30),
	`appartus_price` double ,
	`appartus_number` double ,
	`appartus_productiondate` varchar (33),
	`appartus_productionaddress` varchar (33),
	`appartus_classify` varchar (30)
); 
insert into `appartus` (`appartus_id`, `appartus_name`, `appartus_price`, `appartus_number`, `appartus_productiondate`, `appartus_productionaddress`, `appartus_classify`) values('1','氧气瓶','52','511','2021-02-18','山西阳泉平定县','病房护理设备');
insert into `appartus` (`appartus_id`, `appartus_name`, `appartus_price`, `appartus_number`, `appartus_productiondate`, `appartus_productionaddress`, `appartus_classify`) values('2','洗胃机','569','500','2021-02-18','山西太原市小店区','病房护理设备');
insert into `appartus` (`appartus_id`, `appartus_name`, `appartus_price`, `appartus_number`, `appartus_productiondate`, `appartus_productionaddress`, `appartus_classify`) values('3','注射器','97','888','2021-02-18','中国香港','病房护理设备');
insert into `appartus` (`appartus_id`, `appartus_name`, `appartus_price`, `appartus_number`, `appartus_productiondate`, `appartus_productionaddress`, `appartus_classify`) values('4','体温计','300','212','2021-02-18','中国台湾','物理诊断器具');
insert into `appartus` (`appartus_id`, `appartus_name`, `appartus_price`, `appartus_number`, `appartus_productiondate`, `appartus_productionaddress`, `appartus_classify`) values('5','血压表','5000','200','2021-02-18','中国澳门','物理诊断器具');
insert into `appartus` (`appartus_id`, `appartus_name`, `appartus_price`, `appartus_number`, `appartus_productiondate`, `appartus_productionaddress`, `appartus_classify`) values('6','显微镜','8888','100','2021-02-18','中国新疆','物理诊断器具');
