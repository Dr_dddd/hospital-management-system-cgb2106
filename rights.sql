/*
 Navicat Premium Data Transfer

 Source Server         : fht
 Source Server Type    : MySQL
 Source Server Version : 100307
 Source Host           : localhost:3306
 Source Schema         : hospital

 Target Server Type    : MySQL
 Target Server Version : 100307
 File Encoding         : 65001

 Date: 08/10/2021 17:29:06
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for rights
-- ----------------------------
DROP TABLE IF EXISTS `rights`;
CREATE TABLE `rights`  (
  `id` double DEFAULT NULL,
  `name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `parent_id` double DEFAULT NULL,
  `path` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `level` double DEFAULT NULL,
  `created` datetime(0) DEFAULT NULL,
  `updated` datetime(0) DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rights
-- ----------------------------
INSERT INTO `rights` VALUES (1, '用户管理', 0, NULL, 1, '2021-02-18 19:17:23', '2021-02-18 19:17:23');
INSERT INTO `rights` VALUES (2, '用户列表', 1, '/user', 2, '2021-02-18 19:22:19', '2021-02-18 19:22:19');
INSERT INTO `rights` VALUES (11, '用户列表-新增按钮', 2, NULL, 3, '2021-02-18 19:24:12', '2021-02-18 19:24:12');
INSERT INTO `rights` VALUES (12, '用户列表-修改按钮', 2, NULL, 3, '2021-02-18 19:24:12', '2021-02-18 19:24:12');
INSERT INTO `rights` VALUES (13, '用户列表-删除按钮', 2, NULL, 3, '2021-02-18 19:24:12', '2021-02-18 19:24:12');
INSERT INTO `rights` VALUES (16, '后勤管理', 0, '', 1, '2021-02-18 19:24:12', '2021-02-18 19:24:12');
INSERT INTO `rights` VALUES (17, '病房管理', 16, '/room', 2, '2021-02-18 19:24:12', '2021-02-18 19:24:12');
INSERT INTO `rights` VALUES (18, '病床管理', 16, '/bed', 2, '2021-02-18 19:24:12', '2021-02-18 19:24:12');
INSERT INTO `rights` VALUES (19, '病人管理', 0, NULL, 1, '2021-02-18 19:24:12', '2021-02-18 19:24:12');
INSERT INTO `rights` VALUES (20, '病人管理', 19, '/patient', 2, '2021-02-18 19:24:12', '2021-02-18 19:24:12');
INSERT INTO `rights` VALUES (21, '药品管理', 0, NULL, 1, '2021-02-18 19:24:12', '2021-02-18 19:24:12');
INSERT INTO `rights` VALUES (22, '药品列表', 21, '/potion', 2, '2021-02-18 19:24:12', '2021-02-18 19:24:12');
INSERT INTO `rights` VALUES (23, '部门管理', 0, NULL, 1, '2021-02-18 19:24:12', '2021-02-18 19:24:12');
INSERT INTO `rights` VALUES (24, '部门列表', 23, '/dept', 2, '2021-02-18 19:24:12', '2021-02-18 19:24:12');
INSERT INTO `rights` VALUES (25, '医生管理', 0, NULL, 1, '2021-02-18 19:24:12', '2021-02-18 19:24:12');
INSERT INTO `rights` VALUES (26, '医生列表', 25, '/personnel', 2, '2021-02-18 19:24:12', '2021-02-18 19:24:12');
INSERT INTO `rights` VALUES (27, '仪器管理', 0, NULL, 1, '2021-02-18 19:24:12', '2021-02-18 19:24:12');
INSERT INTO `rights` VALUES (28, '仪器管理', 27, '/instrument', 2, '2021-02-18 19:24:12', '2021-02-18 19:24:12');

SET FOREIGN_KEY_CHECKS = 1;
