/*
SQLyog 企业版 - MySQL GUI v8.14 
MySQL - 5.5.5-10.3.7-MariaDB 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

create table `dept` (
	`dept_id` double ,
	`dept_no` varchar (300),
	`dept_name` varchar (300),
	`dept_manager` varchar (300),
	`dept_vice_manager` varchar (300)
); 
insert into `dept` (`dept_id`, `dept_no`, `dept_name`, `dept_manager`, `dept_vice_manager`) values('1','bm001','中心检查部',' yg001','yg002');
insert into `dept` (`dept_id`, `dept_no`, `dept_name`, `dept_manager`, `dept_vice_manager`) values('2','bm002',' 住院部',' yg003','yg004');
insert into `dept` (`dept_id`, `dept_no`, `dept_name`, `dept_manager`, `dept_vice_manager`) values('3','bm003',' 药剂科部',' yg010',' yg011');
insert into `dept` (`dept_id`, `dept_no`, `dept_name`, `dept_manager`, `dept_vice_manager`) values('4','bm004',' 门诊部',' yg012',' yg013');
insert into `dept` (`dept_id`, `dept_no`, `dept_name`, `dept_manager`, `dept_vice_manager`) values('9','111','111','111','555');
