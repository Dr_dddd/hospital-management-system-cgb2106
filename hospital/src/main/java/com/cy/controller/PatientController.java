package com.cy.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cy.mapper.BedMapper;
import com.cy.mapper.PatientMapper;
import com.cy.pojo.Bed;
import com.cy.pojo.Patient;
import com.cy.service.BedService;
import com.cy.service.PatientService;
import com.cy.service.RoomService;
import com.cy.vo.PageResult;
import com.cy.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/patient")
public class PatientController{
    @Autowired
    private PatientService patientService;
    @Autowired
    private BedMapper bedMapper;
    @Autowired
    private BedService bedService;
    @Autowired
    private PatientMapper patientMapper;
    @Autowired
    private RoomService roomService;

    @GetMapping("/findAll")
    public List<Patient> findAll(){
       return patientService.findAll();
    }

    @GetMapping("/getPatientList")
    public SysResult getPotionList(PageResult pageResult ){
        pageResult = patientService.getPatientList(pageResult);
        return SysResult.success(pageResult);
    }

    @Transactional
    @PostMapping("/addPatient")
    public SysResult addPatient(@RequestBody Patient patient) {
        QueryWrapper<Bed> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("bed_room_id", patient.getRoomId())
                .eq("bed_no", patient.getBedId());
        Bed bed = bedMapper.selectOne(queryWrapper);
        if(bed.getBedJudge()!=true){
            bed.setBedPatientName(patient.getPatientName())
                    .setBedJudge(true);
            bedMapper.update(bed,queryWrapper);
            bedService.updateBedJudge(bed);
            patientService.addPatient(patient);
            return SysResult.success(patient);
        }else
            return SysResult.fail();
    }

    @GetMapping("/updatePatientBtn")
    public SysResult updatePotionBtn(Patient patient){
        Patient patient1 = patientService.updatePotionBtn(patient);
        return SysResult.success(patient1);
    }

//    @PutMapping("/status")
//    public SysResult updateStatus(@RequestBody Patient patient ){
//        patientService.updateStatus(patient);
//        return SysResult.success();
//    }

    @Transactional
    @PutMapping("/updatePatient")
    public SysResult updatePatient(@RequestBody Patient patient){
        //将修改前病人的原有病床信息清空
        Patient patient1 = patientMapper.selectById(patient.getId());
        QueryWrapper<Bed> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("bed_room_id", patient1.getRoomId())
                .eq("bed_no", patient1.getBedId());
        Bed bed = bedMapper.selectOne(queryWrapper);
        bed.setBedPatientName("").setBedJudge(false);
        bedMapper.updateById(bed);
//        roomService.updateRoomJudge(bed);
        roomService.updateRoomJudge(bed);
        //判断修改时,要占用的病床状态(判断其是否可以被占用)
        QueryWrapper<Bed> queryWrapper1 =  new QueryWrapper<>();
        queryWrapper1.eq("bed_room_id", patient.getRoomId())
                .eq("bed_no", patient.getBedId());
        bed = bedMapper.selectOne(queryWrapper1);
        if (bed != null && bed.getBedJudge()==false){//可以占用
            bed.setBedPatientName(patient.getPatientName())
                .setBedJudge(true);
//            bedMapper.updateById(bed);//更新修改后要占用的病床信息
            bedService.updateBedJudge(bed);//更新修改后要占用的病床状态及其病房的状态更新
            patientService.updatePatientById(patient);//修改病人信息
            return SysResult.success(patient);
        }else {
            return SysResult.fail();
        }
    }

    @Transactional
    @DeleteMapping("/delete")
    public SysResult deleteById(Patient patient){

        patientService.deleteById(patient);
        return SysResult.success();
    }

    @GetMapping("/getbedIdList")
    public SysResult getbedIdList(String roomName){

        return patientService.getbedIdList(roomName);
    }


}
