package com.cy.controller;


import com.cy.pojo.Potion;
import com.cy.service.PotionService;
import com.cy.vo.PageResult;
import com.cy.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@CrossOrigin
@RequestMapping("/potion")
public class PotionController {

    @Autowired
    private PotionService potionService;

    @GetMapping("/findAll")
    public List<Potion> findAll(){

        return potionService.findAll();
    }
    /**
     * 参数：pageResult接受
     * 返回值：SysResult对象（pageResult）
     *
     */
    @GetMapping("/list")
    public SysResult getPotionList(PageResult pageResult ){
       pageResult = potionService.getPotionList(pageResult);
       return SysResult.success(pageResult);
    }

    /**
     * 业务说明：修改状态信息
     * URL：/potion/status/{id}/{status}
     * 参数：id/status
     * 返回值 SysResult
     */

    @PutMapping("/status/{potionId}/{potionStatus}")
    public SysResult updateStatus(Potion potion ){
        potionService.updateStatus(potion);
        return SysResult.success();
    }
    /**
     * 业务：实现药品新增
     * url：/potion/addPotion
     * 参数：整个form表单  对象  json
     * 返回值 SysResult对象
     * */

    @PostMapping("/addPotion")
    public SysResult addPotion(@RequestBody Potion potion) {
        potionService.addPotion(potion);
        return SysResult.success();
    }

    @DeleteMapping("/delete/{potionId}")
        public SysResult deleteById(Potion potion){

            potionService.deleteById(potion);
            return SysResult.success();
    }

    @GetMapping("/updatePotionBtn/{potionId}")
    public SysResult updatePotionBtn(Potion potion){
        Potion potion1 = potionService.selectById(potion);
        return SysResult.success(potion1);
    }

    @PutMapping("/updatePotion")
        public SysResult updatePotion( @RequestBody Potion potion){
            potionService.updatePotionById(potion);
            return SysResult.success();
    }


}
