package com.cy.controller;

import com.cy.pojo.Rights;
import com.cy.service.RightsService;
import com.cy.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/rights")
public class RightsController {
    @Autowired
    private RightsService rightsService;

    @GetMapping("/findAll")
    public List<Rights> findAll(){
        return rightsService.findAll();
    }

    /**
     * 查询左侧菜单列表
     * URL: /rights/getRightsList
     * 参数: 没有参数
     * 返回值: SysResult
     */
    @GetMapping("/getRightsList")
    public SysResult getRightList(){
        List<Rights> rightsList = rightsService.getRightsList();
         return SysResult.success(rightsList);
    }


}
