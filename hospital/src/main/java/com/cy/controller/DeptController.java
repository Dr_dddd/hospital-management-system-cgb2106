package com.cy.controller;

import com.cy.pojo.Dept;
import com.cy.service.DeptService;
import com.cy.vo.PageResult;
import com.cy.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/dept")
@CrossOrigin
public class DeptController {
    @Autowired
    private DeptService deptService;
    @GetMapping("/findAll")
    public List<Dept> findAlll(){
        return deptService.findAll();
    }
    @GetMapping("/list")
    public SysResult list(PageResult pageResult){
        pageResult=deptService.list(pageResult);
        return SysResult.success(pageResult);
    }
    @PostMapping("/addDept")
    public SysResult addDept(@RequestBody Dept dept){
        deptService.addDept(dept);
        return SysResult.success();
    }
    @DeleteMapping("/delete/{deptId}")
    public SysResult delete(@PathVariable Integer deptId){
        deptService.delete(deptId);
        return SysResult.success();
    }
    @GetMapping("/getById")
    public SysResult getById(Integer deptId){
        System.out.println("======================="+deptId);
        Dept dept=deptService.getById(deptId);
        System.out.println("++++++++++++++++++++++++++++++"+dept);
        return SysResult.success(dept);
    }
    @PutMapping("/updateDept")
    public SysResult updateDept(@RequestBody Dept dept){
        System.out.println("========================"+dept);
        deptService.updateDept(dept);
        return SysResult.success();
    }
}
