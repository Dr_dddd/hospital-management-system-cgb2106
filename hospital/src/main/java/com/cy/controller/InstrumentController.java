package com.cy.controller;


import com.cy.pojo.Appartus;
import com.cy.service.InstrumentService;
import com.cy.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/instrument")
public class InstrumentController {
    @Autowired
    private InstrumentService instrumentService;

    @GetMapping("/list")
    public SysResult getInstrumentList(){
        List<Appartus> instrumentList = instrumentService.getInstrumentList();

        return SysResult.success(instrumentList);
    }
    @GetMapping("/{appartusId}")
    public SysResult update1(@PathVariable Integer appartusId ){
        Appartus appartus =instrumentService.update1(appartusId);
        return SysResult.success(appartus);
    }
    @PutMapping("/updateInstrument")
    public  SysResult updateInstrument(@RequestBody Appartus appartus){
        instrumentService.updateInstrument(appartus);
        return SysResult.success();

    }
    @DeleteMapping("/delete")
    public  SysResult delete(Integer appartusId){
        instrumentService.delete(appartusId);
        return SysResult.success();

    }
    @PostMapping("/addInstrument")
    public SysResult addInstrument(@RequestBody Appartus appartus){
        instrumentService.addInstrument(appartus);
        return SysResult.success();
    }

}
