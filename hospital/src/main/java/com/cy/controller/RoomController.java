package com.cy.controller;

import com.cy.pojo.Room;
import com.cy.service.RoomService;
import com.cy.vo.PageResult;
import com.cy.vo.SysResult;
import org.apache.ibatis.annotations.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/room")
public class RoomController {

    @Autowired
    private RoomService roomService;

    @GetMapping("selectAll")
    public List<Room> selectAll(Room room){
        return roomService.selectAll(room);
    }

    @GetMapping("/getRoomList")
    public SysResult getRoomList(PageResult queryRoomInfo){
        queryRoomInfo = roomService.getRoomList(queryRoomInfo);
        return SysResult.success(queryRoomInfo);
    }

    @GetMapping
    public List<Room> selectByRoomId(Room room){
        return roomService.selectByRoomId(room);
    }

    @Transactional
    @PostMapping("/addRoom")
    public SysResult addRoom(@RequestBody Room addRoomModel){
        List<Room> rooms = roomService.selectByRoomId(addRoomModel);
        if(rooms.size()==0){
            roomService.addRoom(addRoomModel);
            return SysResult.success(addRoomModel);
        }
        return SysResult.fail();
    }

//    @PutMapping("/updateRoomJudge")
//    public Room updateRoomJudge(Room room){
//        roomService.updateRoomJudge(room);
//        return room;
//    }

    @GetMapping("/{roomId}")
    public SysResult getRoom(@PathVariable Integer roomId){
        Room room =roomService.getRoom(roomId);
        return SysResult.success(room);
    }

    /**业务说明:病房数据的修改
     * 请求路径:/room/updateRoom
     */
    @Transactional
    @PutMapping("/updateRoom")
    public SysResult updateRoom(@RequestBody Room room){
        if(roomService.updateRoomif(room)==true){
            roomService.updateRoom(room);
            return SysResult.success();
        }else
            return SysResult.fail();
    }
    //    room/deleteRoomById

    @Transactional
    @DeleteMapping("/deleteRoomById")
    public SysResult deleteRoomById(Room room){
        if(room.getBedUsed()==0){
            roomService.deleteRoomById(room);
            return SysResult.success();
        }else
            return SysResult.fail();
    }

    @GetMapping("/getRoomNoList")
    public List<Room> getRoomNoList(){
        return roomService.getRoomNoList();
    }
}

