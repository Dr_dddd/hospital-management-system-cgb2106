package com.cy.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@PropertySource("classpath:/application.yml")
public class PortController {
    @Value("${server.port}")
    private Integer port;
    @GetMapping("/getPort")
    public String getPort(){
        return  "访问的端口为"+port;
    }
}
