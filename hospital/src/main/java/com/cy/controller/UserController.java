package com.cy.controller;

import com.cy.pojo.User;
import com.cy.service.UserService;
import com.cy.vo.PageResult;
import com.cy.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author 刘昱江
 * 时间 2021/5/11
 */
@RestController
@CrossOrigin
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/hello")
    public List<User> hello(){

        return userService.findAll();
    }

    @PutMapping("/updatePassword")
    public SysResult updatePassword(@RequestBody User user){
        String s = userService.updatePassword(user);
        if(s =="202"){
            return new SysResult(202,"业务执行失败","null");
        }
        return SysResult.success();
    }

    @RequestMapping("/login")
    public SysResult login(@RequestBody User user){
        String token = userService.login(user);
        if (token == null|| token.length() == 0){
            return SysResult.fail();
        }
        if(token == "222"){
            return new SysResult(202,"业务执行失败","null");
        }
        return SysResult.success(token);
    }

    /**
     * 业务说明:实现用户列表的分页
     * URL地址:http://localhost:8091/user/list?query=&pageNum=1&pageSize=10
     * 参数: pageResult
     * 返回值: SysResult对象(pageResult)
     * */
    @GetMapping("/list")
    public SysResult getUserList(PageResult pageResult){
        pageResult = userService.getUserList(pageResult);
        return SysResult.success(pageResult);
    }

    /**
     * 业务说明: 修改状态信息
     * URL:  /user/status/{id}/{status}
     * 参数:  id/status
     * 返回值:  SysResult
     */
    @PutMapping("/status/{id}/{status}")
    public SysResult updateStatus(User user){
        userService.updateStatus(user);
        return SysResult.success(user);
    }

    /**
     *
     */
    @PostMapping("/addUser")
    public SysResult addUser(@RequestBody User user){
        userService.addUser(user);
        return SysResult.success();
    }
    /**
     * 业务: 根据ID删除用户数据
     * URL: /user/{id}
     * 参数: 主键id
     * 返回值: SysResult(user对象)
     */

    @DeleteMapping("/{id}")
    public SysResult deleteUser(@PathVariable Integer id){
        userService.deleteUser(id);
        return SysResult.success();
    }

    /**
     * 业务: 根据id查询数据库
     * URL: /user/id
     * 参数: 主键id
     * 返回值: SysResult(User 对象)
     */
    @GetMapping("/{id}")
    public SysResult findById(@PathVariable Integer id){
        User user =  userService.findById(id);
        return SysResult.success(user);
    }
    /**
     * 业务需求:实现用户功能新操作
     * URL: /user/updateUser
     * 参数: 对象提交JSON字符串 注意接受
     * 返回值SysResult
     */

    @PutMapping("/updateUser")
    public SysResult updateUser(@RequestBody User user){
        userService.updateUser(user);
        return SysResult.success();
    }



}
