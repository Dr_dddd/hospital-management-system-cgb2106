package com.cy.controller;

import com.cy.pojo.Bed;
import com.cy.service.BedService;
import com.cy.vo.PageResult;
import com.cy.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/bed")
public class BedController {

    @Autowired
    private BedService bedService;

    @GetMapping("/getBedList")
    public SysResult getBedList(PageResult queryBedInfo){
        queryBedInfo = bedService.getBedList(queryBedInfo);
        return SysResult.success(queryBedInfo);
    }

    ///bed/updateBedJudge
    @Transactional
    @PutMapping("/updateBedJudge")
    public SysResult updateBedJudge(@RequestBody Bed bed){
        bedService.updateBedJudge(bed);
        return SysResult.success();
    }

    @Transactional
    @PostMapping
    public Bed addByBedRoomNo(Bed bed){
        bedService.addByBedRoomNo(bed);
        return bed;
    }

}
