package com.cy.controller;

import com.cy.pojo.Personnel;
import com.cy.service.PersonnelService;
import com.cy.vo.PageResult;
import com.cy.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RequestMapping("/personnel")
@RestController
public class PersonnelController {
    @Autowired
    private PersonnelService personnelService;
    @GetMapping("/findAll")
    public List<Personnel> findAlll(){
        return personnelService.findAll();
    }
    @GetMapping("/list")
    public SysResult list(PageResult pageResult){
        pageResult=personnelService.list(pageResult);
        return SysResult.success(pageResult);
    }
    @PutMapping("/empHealth/{empId}/{empHealth}")
    public SysResult putStutas(Personnel personnel){
        personnelService.putStutas(personnel);
        return SysResult.success();
    }
    @PostMapping("/addPersonnel")
    public SysResult addUser(@RequestBody Personnel personnel){
        personnelService.addPersonnel(personnel);
        return SysResult.success();
    }
    @DeleteMapping("/delete/{empId}")
    public SysResult delete(@PathVariable("empId") Integer empId){
        personnelService.delete(empId);
        return SysResult.success();
    }
    @GetMapping("/getById")
    public SysResult getById(Integer empId){
        Personnel personnel=personnelService.getById(empId);
        return SysResult.success(personnel);
    }
    @PutMapping("/updatePersonnel")
    public SysResult updateUser(@RequestBody Personnel personnel){
        System.out.println("---------------"+personnel);
        personnelService.updateUser(personnel);
        return SysResult.success();
    }

}
