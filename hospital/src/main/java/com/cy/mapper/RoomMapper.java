package com.cy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cy.pojo.Room;

public interface RoomMapper extends BaseMapper<Room> {
}
