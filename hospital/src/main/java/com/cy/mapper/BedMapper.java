package com.cy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cy.pojo.Bed;

public interface BedMapper extends BaseMapper<Bed> {
}
