package com.cy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.cy.pojo.Potion;


public interface PotionMapper extends BaseMapper<Potion> {

   /* //注解方式查询数据库，只适用于简单的sql查询  注解/映射文件二选一
    @Select("select * from user limit #{start},#{size}")
    List<User> findListByPage(int start, int size);*/
}
