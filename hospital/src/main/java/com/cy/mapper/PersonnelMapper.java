package com.cy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cy.pojo.Personnel;

public interface PersonnelMapper extends BaseMapper<Personnel> {
}
