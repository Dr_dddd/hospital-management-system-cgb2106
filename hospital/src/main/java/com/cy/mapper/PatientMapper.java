package com.cy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cy.pojo.Patient;

public interface PatientMapper extends BaseMapper<Patient> {
}
