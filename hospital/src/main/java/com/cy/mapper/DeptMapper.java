package com.cy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cy.pojo.Dept;

public interface DeptMapper extends BaseMapper<Dept> {
}
