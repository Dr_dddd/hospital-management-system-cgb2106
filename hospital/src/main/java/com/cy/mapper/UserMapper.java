package com.cy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cy.pojo.User;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author 刘昱江
 * 时间 2021/2/2
 */
public interface UserMapper extends BaseMapper<User> {
    @Select("select * from user limit #{start},#{size}")
    List<User> findByPage(int start, int size);
}
