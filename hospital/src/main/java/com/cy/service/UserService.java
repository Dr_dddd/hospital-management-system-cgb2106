package com.cy.service;

import com.cy.pojo.User;
import com.cy.vo.PageResult;

import java.util.List;

/**
 * @author 刘昱江
 * 时间 2021/5/11
 */
public interface UserService {
    List<User> findAll();

    String login(User user);

    PageResult getUserList(PageResult pageResult);

    void updateStatus(User user);

    void addUser(User user);

    void deleteUser(Integer id);

    User findById(Integer id);

    void updateUser(User user);

    String updatePassword(User user);
}
