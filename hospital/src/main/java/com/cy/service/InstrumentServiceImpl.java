package com.cy.service;


import com.cy.mapper.InstrumentMapper;
import com.cy.pojo.Appartus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InstrumentServiceImpl implements InstrumentService{
    @Autowired
    private InstrumentMapper instrumentMapper;
    @Override
    public List<Appartus> getInstrumentList() {
        return instrumentMapper.selectList(null);
    }
    @Override
    public Appartus update1(Integer appartusId) {
        return instrumentMapper.selectById(appartusId);

    }

    @Override
    public void updateInstrument(Appartus appartus) {
        instrumentMapper.updateById(appartus);
    }

    @Override
    public void delete(Integer appartusId) {
        instrumentMapper.deleteById(appartusId);
    }

    @Override
    public void addInstrument(Appartus appartus) {
        instrumentMapper.insert(appartus);

    }


}
