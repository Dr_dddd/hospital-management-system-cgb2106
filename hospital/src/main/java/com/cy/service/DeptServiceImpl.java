package com.cy.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cy.mapper.DeptMapper;
import com.cy.pojo.Dept;
import com.cy.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class DeptServiceImpl implements DeptService{
    @Autowired
    private DeptMapper deptMapper;
    @Override
    public List<Dept> findAll() {
        return deptMapper.selectList(null);
    }

    @Override
    public PageResult list(PageResult pageResult) {
        IPage<Dept> page = new Page<>(pageResult.getPageNum(),pageResult.getPageSize());
        Boolean flag= StringUtils.hasLength(pageResult.getQuery());
        QueryWrapper<Dept> queryWrapper = new QueryWrapper<>();
        queryWrapper.like(flag,"dept_name",pageResult.getQuery());
        deptMapper.selectPage(page,queryWrapper);
        pageResult.setTotal(page.getTotal()).setRows(page.getRecords());
        return pageResult;
    }

    @Override
    public void addDept(Dept dept) {
        deptMapper.insert(dept);
    }

    @Override
    public void delete(Integer deptId) {
        deptMapper.deleteById(deptId);
    }

    @Override
    public Dept getById(Integer deptId) {
        return deptMapper.selectById(deptId);

    }

    @Override
    public void updateDept(Dept dept) {
        deptMapper.updateById(dept);
    }

}
