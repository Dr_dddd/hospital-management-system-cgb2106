package com.cy.service;




import com.cy.pojo.Appartus;

import java.util.List;

public interface InstrumentService {
    List<Appartus> getInstrumentList();

    Appartus update1(Integer appartusId);


    void updateInstrument(Appartus appartus);


    void delete(Integer appartusId);

    void addInstrument(Appartus appartus);
}
