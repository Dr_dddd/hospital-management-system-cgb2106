package com.cy.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cy.mapper.BedMapper;
import com.cy.mapper.RoomMapper;
import com.cy.pojo.Bed;
import com.cy.pojo.Room;
import com.cy.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import java.util.List;

@Service
public class RoomServiceImpl implements RoomService {

    static final String[] str = {"二人间", "四人间", "六人间"};

    @Autowired
    private RoomMapper roomMapper;

    @Autowired
    private BedMapper bedMapper;

    @Autowired
    private BedService bedService;

    @Override
    public PageResult getRoomList(PageResult queryRoomInfo) {
        //1.定义分页对象
        IPage<Room> page = new Page<>(queryRoomInfo.getPageNum(), queryRoomInfo.getPageSize());
        //2.定义条件构造器
        boolean flag = StringUtils.hasLength(queryRoomInfo.getQuery());
        QueryWrapper<Room> queryWrapper = new QueryWrapper<>();
        queryWrapper.like(flag, "room_no", queryRoomInfo.getQuery())
                .or()
                .like(flag, "room_grade", queryRoomInfo.getQuery());
        //3.进行分页查询
        page = roomMapper.selectPage(page, queryWrapper);
        //4.从封装后的分页对象中获取数据
        queryRoomInfo.setTotal(page.getTotal()).setRows(page.getRecords());
        return queryRoomInfo;
    }

    @Override
    public List<Room> selectAll(Room room) {
        return roomMapper.selectList(null);
    }

    @Override
    public List<Room> selectByRoomId(Room room) {
        QueryWrapper<Room> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("room_id", room.getRoomNo());
        return roomMapper.selectList(queryWrapper);
    }

///    @Transactional
//    @Override
//    public void updateRoomJudge(Room room) {
//        if(room.getRoomBedempty()<=0)
//            roomMapper.updateById(room);
//    }

    @Override
    public Room getRoom(Integer roomId) {
        return roomMapper.selectById(roomId);
    }

    @Transactional
    @Override
    public void updateRoom(Room room) {
        Room room1 = roomMapper.selectById(room.getRoomId());
        int used = room1.getBedUsed();
        int type = room.getRoomType();
        room.setRoomBedempty(type * 2 - used)
                .setBedUsed(used).setRoomGrade(str[type-1]);
        roomMapper.updateById(room);
    }

    @Override
    public boolean updateRoomif(Room room) {
        Room room1 = roomMapper.selectById(room.getRoomId());
        int used = room1.getBedUsed();
        int type = room.getRoomType();//改之后
        System.out.println(type);
        int type1 = room1.getRoomType();//改之前
        System.out.println(type1);
        Bed bed = new Bed();
        if (used <= type * 2) {
            if(type1<=type){
                for(int i=type1*2+1;i<=type*2;i++){
                    bed.setBedRoomId(room.getRoomNo()).setBedNo("bc00"+i)
                            .setBedPatientName(null).setBedJudge(false);
                    bedMapper.insert(bed);
                }
            }else{
                for(int i=type1*2;i>type*2;i--){
                    QueryWrapper<Bed> queryWrapper = new QueryWrapper<>();
                    queryWrapper.eq("bed_room_id", room.getRoomNo())
                            .eq("bed_no", "bc00"+i);
                    bedMapper.delete(queryWrapper);
                }
            }
            return true;
        }else
            return false;
    }


    @Transactional
    @Override
    public void deleteRoomById(Room room) {
        QueryWrapper<Bed> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("bed_room_id", room.getRoomNo());
        bedMapper.delete(queryWrapper);
        roomMapper.deleteById(room.getRoomId());
    }

    @Transactional
    @Override
    public void addRoom(Room addRoomModel) {
        int type = addRoomModel.getRoomType();
        addRoomModel.setRoomBedempty(type*2).setRoomGrade(str[type-1]).setRoomJudge(false);
        roomMapper.insert(addRoomModel);
        for(int i=1;i<=addRoomModel.getRoomType()*2;i++){
            Bed bed = new Bed();
            bed.setBedRoomId(addRoomModel.getRoomNo()).setBedNo("bc00"+i).setBedJudge(false);
            bedService.addByBedRoomNo(bed);
        }
    }
    @Override
    public List<Room> getRoomNoList() {
        QueryWrapper<Room> queryWrapper = new QueryWrapper<>();
//        queryWrapper.select("room_no");
        queryWrapper.eq("room_judge", 0);
        return roomMapper.selectList(queryWrapper);
    }

    public void updateRoomJudge(Bed bed){
        QueryWrapper<Room> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("room_no", bed.getBedRoomId());
        Room room = roomMapper.selectOne(queryWrapper);
        if(bed.getBedJudge()==true) {
            room.setBedUsed(room.getBedUsed()+1);
            room.setRoomBedempty(room.getRoomBedempty() - 1);
        }else {
            room.setBedUsed(room.getBedUsed()-1);
            room.setRoomBedempty(room.getRoomBedempty() + 1);
        }
        if(room.getRoomBedempty()==0){
            room.setRoomJudge(true);
        }else
            room.setRoomJudge(false);
        roomMapper.updateById(room);
    }

}
