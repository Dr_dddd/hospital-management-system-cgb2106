package com.cy.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import com.cy.mapper.RightsMapper;
import com.cy.pojo.Rights;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Service
public class RightsServiceImpl implements RightsService {
    @Autowired
    private RightsMapper rightsMapper;
    @Override
    public List<Rights> findAll() {

        return rightsMapper.selectList(null);
    }

    @Override
    public List<Rights> getRightsList() {
        QueryWrapper<Rights> qw = new QueryWrapper<Rights>();
        qw.eq("parent_id", 0);
        List<Rights> rightsList = rightsMapper.selectList(qw);
        for (Rights oneRights : rightsList){
            qw.clear();
            qw.eq("parent_id", oneRights.getId());
            List<Rights> twolist = rightsMapper.selectList(qw);
            oneRights.setChildren(twolist);
        }
        return rightsList;
    }


}
