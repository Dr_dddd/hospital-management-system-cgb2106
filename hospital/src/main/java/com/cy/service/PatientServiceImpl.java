package com.cy.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cy.mapper.BedMapper;
import com.cy.mapper.PatientMapper;
import com.cy.pojo.Bed;
import com.cy.pojo.Patient;

import com.cy.vo.PageResult;
import com.cy.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class PatientServiceImpl implements PatientService{
    @Autowired
    private PatientMapper patientMapper;
    @Autowired
    private BedMapper bedMapper;
    @Autowired
    private BedService bedService;
    @Override
    public List<Patient> findAll() {

        return patientMapper.selectList(null);
    }

    @Override
    public PageResult getPatientList(PageResult pageResult) {
        IPage<Patient> page = new Page<>(pageResult.getPageNum(),pageResult.getPageSize());
        //2.定义条件构造器
        boolean flag = StringUtils.hasLength(pageResult.getQuery());
        QueryWrapper<Patient> queryWrapper = new QueryWrapper<>();

        queryWrapper.like(flag,"patient_name",pageResult.getQuery())
                .or()
            .eq(flag,"room_id", pageResult.getQuery())
                .or()
            .eq(flag,"bed_id", pageResult.getQuery());

        //3.进行分页查询
        page = patientMapper.selectPage(page,queryWrapper);
        //4.从封装后的的分页对象中获取数据
        pageResult.setTotal(page.getTotal()).setRows(page.getRecords());

        return pageResult;
    }

    @Transactional
    @Override
    public void addPatient(Patient patient) {
        patientMapper.insert(patient);
    }

    @Override
    public Patient updatePotionBtn(Patient patient) {
        System.out.println(patient);
        QueryWrapper<Bed> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("bed_room_id",patient.getRoomId())
                .eq("bed_no", patient.getBedId());
        Bed bed = bedMapper.selectOne(queryWrapper);
        System.out.println(bed);
        return patientMapper.selectById(patient);
    }

//    @Transactional
//    @Override
//    public void updateStatus(Patient patient) {
//        System.out.println(patient);
//        QueryWrapper<Bed> queryWrapper = new QueryWrapper<>();
//        queryWrapper.eq("bed_room_id", patient.getRoomId())
//                .eq("bed_no", patient.getBedId());
//        Bed bed = bedMapper.selectOne(queryWrapper);
//        bed.setBedJudge(patient.getStatus());
//        bedMapper.updateById(bed);
//        patientMapper.updateById(patient);
//    }

    @Transactional
    @Override
    public void updatePatientById(Patient patient) {
        patientMapper.updateById(patient);
    }

    @Transactional
    @Override
    public void deleteById(Patient patient) {
        QueryWrapper<Bed> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("bed_room_id", patient.getRoomId())
                .eq("bed_no", patient.getBedId());
        Bed bed = bedMapper.selectOne(queryWrapper);
        bed.setBedPatientName("").setBedJudge(false);
        bedMapper.update(bed,queryWrapper);
        bedService.updateBedJudge(bed);
        patientMapper.deleteById(patient);
    }

    @Override
    public SysResult getbedIdList(String roomName) {
        QueryWrapper<Bed> bedQueryWrapper = new QueryWrapper<>();
        bedQueryWrapper.eq("bed_room_id", roomName)
        .eq("bed_judge", 0);
        List<Bed> beds = bedMapper.selectList(bedQueryWrapper);
        return SysResult.success(beds);
    }


}
