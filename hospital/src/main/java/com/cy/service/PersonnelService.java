package com.cy.service;

import com.cy.pojo.Personnel;
import com.cy.vo.PageResult;

import java.util.List;

public interface PersonnelService {
    List<Personnel> findAll();

    PageResult list(PageResult pageResult);

    void putStutas(Personnel personnel);

    void addPersonnel(Personnel personnel);


    void delete(Integer empId);

    Personnel getById(Integer empId);

    void updateUser(Personnel personnel);
}
