package com.cy.service;


import com.cy.pojo.Potion;
import com.cy.vo.PageResult;

import java.util.List;


public interface PotionService {
    List<Potion> findAll();

   // String login(Potion potion);

    PageResult getPotionList(PageResult pageResult);

    void updateStatus(Potion potion);

    void addPotion(Potion potion);

    void deleteById(Potion potion);

    Potion selectById(Potion potion);

    void updatePotionById(Potion potion);
}
