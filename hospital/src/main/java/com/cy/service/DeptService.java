package com.cy.service;

import com.cy.pojo.Dept;
import com.cy.vo.PageResult;

import java.util.List;


public interface DeptService {
    List<Dept> findAll();

    PageResult list(PageResult pageResult);

    void addDept(Dept dept);

    void delete(Integer deptId);

    Dept getById(Integer deptId);

    void updateDept(Dept dept);
}
