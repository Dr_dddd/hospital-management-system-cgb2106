package com.cy.service;

import com.cy.pojo.Bed;
import com.cy.pojo.Room;
import com.cy.vo.PageResult;
import java.util.List;

public interface RoomService {
    PageResult getRoomList(PageResult queryRoomInfo);

    List<Room> selectAll(Room room);

    void addRoom(Room addRoomModel);

    List<Room> selectByRoomId(Room room);

    void updateRoomJudge(Bed bed);

    Room getRoom(Integer roomId);

    void updateRoom(Room room);

    void deleteRoomById(Room room);

    boolean updateRoomif(Room room);

    List<Room> getRoomNoList();
}