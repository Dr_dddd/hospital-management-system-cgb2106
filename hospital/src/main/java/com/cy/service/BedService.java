package com.cy.service;

import com.cy.pojo.Bed;
import com.cy.vo.PageResult;

public interface BedService {

    PageResult getBedList(PageResult queryBedInfo);

    void updateBedJudge(Bed bed);

    void addByBedRoomNo(Bed bed);
}
