package com.cy.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cy.mapper.BedMapper;
import com.cy.mapper.RoomMapper;
import com.cy.pojo.Bed;
import com.cy.pojo.Room;
import com.cy.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class BedServiceImpl implements BedService {

    @Autowired
    private BedMapper bedMapper;

    @Autowired
    private RoomMapper roomMapper;

    @Autowired
    private RoomService roomService;

    @Override
    public PageResult getBedList(PageResult queryBedInfo) {
        //1.定义分页对象
        IPage<Bed> page = new Page<>(queryBedInfo.getPageNum(),queryBedInfo.getPageSize());
        //2.定义条件构造器

        boolean flag = StringUtils.hasLength(queryBedInfo.getQuery());
        QueryWrapper<Bed> queryWrapper = new QueryWrapper<>();
        queryWrapper.like(flag, "bed_room_id", queryBedInfo.getQuery());
        //3.进行分页查询
        page = bedMapper.selectPage(page,queryWrapper);
        //4.从封装后的分页对象中获取数据
        queryBedInfo.setTotal(page.getTotal()).setRows(page.getRecords());
        return queryBedInfo;
    }

    @Override
    public void updateBedJudge(Bed bed) {
        bedMapper.updateById(bed);
//        QueryWrapper<Room> queryWrapper = new QueryWrapper<>();
//        queryWrapper.eq("room_no", bed.getBedRoomId());
//        Room room = roomMapper.selectOne(queryWrapper);
//        if(bed.getBedJudge()==true) {
//            room.setBedUsed(room.getBedUsed()+1);
//            room.setRoomBedempty(room.getRoomBedempty() - 1);
//        }else {
//            room.setBedUsed(room.getBedUsed()-1);
//            room.setRoomBedempty(room.getRoomBedempty() + 1);
//        }
//        if(room.getRoomBedempty()==0){
//            room.setRoomJudge(true);
//        }else
//            room.setRoomJudge(false);
//        roomMapper.updateById(room);
        roomService.updateRoomJudge(bed);
    }

    @Override
    public void addByBedRoomNo(Bed bed) {
        bedMapper.insert(bed);
    }

}

