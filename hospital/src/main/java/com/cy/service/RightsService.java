package com.cy.service;

import com.cy.pojo.Rights;

import java.util.List;

public interface RightsService {
    List<Rights> findAll();

    List<Rights> getRightsList();
}
