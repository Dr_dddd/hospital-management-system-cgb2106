package com.cy.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.cy.mapper.PotionMapper;
import com.cy.pojo.Potion;
import com.cy.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;


/**
 * @author 刘昱江
 * 时间 2021/5/11
 */
@Service
public class PotionServiceImpl implements PotionService{

    @Autowired
    private PotionMapper potionMapper;

    @Override
    @Transactional
    public List<Potion> findAll() {
        return potionMapper.selectList(null);
    }

//    @Override
//    @Transactional
//    public String login(Potion potion) {
//        //明文
//        String password = potion.();
//        //加密
//        String md5 = DigestUtils.md5DigestAsHex(password.getBytes());
//        potion.setPassword(md5);
//        QueryWrapper<Potion> qw1 = new QueryWrapper<>(potion);
//        Potion potionDB = potionMapper.selectOne(qw1);
//        if (potionDB == null){
//            return null;
//        }
//        //使用UUID动态生成token根据当前时间毫秒数+随机数利用hash算法生成
//        //几乎保证不重复
//
//        String token= UUID.randomUUID().toString().replace("-", "");
//            return token;
//    }
    /**
     * 分页Sql：
     *      语法：select* from potion limit 起始位置，每页条数
     *      查询第一页
     *              select *potion from limit 0,10
     *      查询第二页
     *              select *potion from limit 10,10
     */
     /*    @Override
    public PageResult getPotionList(PageResult pageResult) {
        //1.获取记录总数  Integer --> long  自动转换
        long total = potionMapper.selectCount(null);
        //2.获取分页结果
        //2.1获取条数
        int size = pageResult.getPageSize();
        //2.2获取开始页数
        int start = (pageResult.getPageNum()-1)*size;
        //3.查询
        List<Potion> potionList= potionMapper.findListByPage(start,size);
            pageResult.setTotal(total).setRows(potionList);
        return pageResult;
    }*/

    /**
     * 业务说明：利用MP方式查询数据库
     * 步骤梳理：
     *          1.构建MP的分页对象
     *          2.跟据分页对象查询数据
     *          3.从分页对象中获取数据
     *          4.封装PageResult对象
     *          5.编辑配置类，封装分页拦截器
     * @param pageResult
     * @return
     */
    @Override
    @Transactional
    public PageResult getPotionList(PageResult pageResult) {
        //1.定义分页对象
        IPage<Potion> page = new Page<>(pageResult.getPageNum(),pageResult.getPageSize());
        //2.定义条件构造器
        boolean flag = StringUtils.hasLength(pageResult.getQuery());
        QueryWrapper<Potion> queryWrapper = new QueryWrapper<>();

             queryWrapper.like(flag,"potion_name",pageResult.getQuery());

        //3.进行分页查询
         page = potionMapper.selectPage(page, queryWrapper);
         //4.从封装后的的分页对象中获取数据
        pageResult.setTotal(page.getTotal()).setRows(page.getRecords());

        return pageResult;
    }


    @Override
    @Transactional
    public void updateStatus(Potion potion) {
        potionMapper.updateById(potion);
    }
    /**
    * 1.用户入库操作需要手动补齐的数据有
    * 创建时间/修改时间  保证一致
    * status设为 true
    * 2.密码加密处理，新增和登录的加密方法一致*/
    @Override
    @Transactional
    public void addPotion(Potion potion) {
        potion.setPotionStatus(true).setCreated(new Date()).setUpdated(potion.getCreated());
       // String password = potion.getPassword();
       // password = DigestUtils.md5DigestAsHex(password.getBytes());
        //potion.setPassword(password);
        potionMapper.insert(potion);
    }


    @Override
    @Transactional
    public void deleteById(Potion potion) {
        potionMapper.deleteById(potion);
    }

    @Override
    @Transactional
    public Potion selectById(Potion potion) {
        Potion potion1 = potionMapper.selectById(potion);
        return potion1;

    }

    @Override
    @Transactional
    public void updatePotionById(Potion potion) {

        potionMapper.updateById(potion);
    }



}
