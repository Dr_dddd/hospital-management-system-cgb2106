package com.cy.service;

import com.cy.pojo.Patient;
import com.cy.vo.PageResult;
import com.cy.vo.SysResult;
import com.cy.pojo.Bed;

import java.util.List;

public interface PatientService {
    List<Patient> findAll();

    PageResult getPatientList(PageResult pageResult);

    void addPatient(Patient patient);

    Patient updatePotionBtn(Patient patient);


//    void updateStatus(Patient patient);

    void updatePatientById(Patient patient);

    void deleteById(Patient patient);

    SysResult getbedIdList(String roomName);
}
