package com.cy.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cy.mapper.PersonnelMapper;
import com.cy.pojo.Personnel;
import com.cy.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class PersonnelServiceImpl implements PersonnelService{
    @Autowired
    private PersonnelMapper personnelMapper;
    @Override
    public List<Personnel> findAll() {
        return personnelMapper.selectList(null);
    }
    @Override
    public PageResult list(PageResult pageResult) {
        IPage<Personnel> page = new Page<>(pageResult.getPageNum(),pageResult.getPageSize());
        Boolean flag= StringUtils.hasLength(pageResult.getQuery());
        QueryWrapper<Personnel> queryWrapper = new QueryWrapper<>();
        queryWrapper.like(flag,"emp_name",pageResult.getQuery());
        personnelMapper.selectPage(page,queryWrapper);
        pageResult.setTotal(page.getTotal()).setRows(page.getRecords());
        return pageResult;
    }

    @Override
    public void putStutas(Personnel personnel) {
        personnelMapper.updateById(personnel);
    }

    @Override
    public void addPersonnel(Personnel personnel) {
        personnelMapper.insert(personnel);
    }

    @Override
    public void delete(Integer empId) {
        personnelMapper.deleteById(empId);
    }

    @Override
    public Personnel getById(Integer empId) {
        return personnelMapper.selectById(empId);
    }

    @Override
    public void updateUser(Personnel personnel) {
        personnelMapper.updateById(personnel);
    }


}
