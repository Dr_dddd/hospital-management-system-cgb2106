package com.cy.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.cy.mapper.UserMapper;
import com.cy.pojo.User;
import com.cy.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.UUID;

/**
 * @author 刘昱江
 * 时间 2021/5/11
 */
@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserMapper userMapper;

    @Override
    public List<User> findAll() {
        System.out.println("ddawjkdhawikj");
        return userMapper.selectList(null);
    }

    @Override
    public String updatePassword(User user) {
        String username = user.getUsername();
        String password = user.getPassword();
        String md5 = DigestUtils.md5DigestAsHex(password.getBytes());
        user.setPassword(md5);
        System.out.println(md5);
        QueryWrapper<User> QueryWrapper = new QueryWrapper<>();
        QueryWrapper.eq("username",username).eq("phone",user.getPhone());
        User user1 = userMapper.selectOne(QueryWrapper);
        if(user1 == null){
            return "202";
        }
        userMapper.update(user,QueryWrapper);
        return "修改成功";
    }

    @Override
    public String login(User user) {
        user.setPassword(DigestUtils.md5DigestAsHex(user.getPassword().getBytes()));
        QueryWrapper<User> qw = new QueryWrapper<>(user);
        User userDB = userMapper.selectOne(qw);
        if (userDB == null){
            return null;
        }
        if(userDB.getStatus() == false){
            return "222";
        }
        return UUID.randomUUID().toString().replace("-", "");
    }

    @Override
    public PageResult getUserList(PageResult pageResult) {

        /*long total = userMapper.selectCount(null);
        int size = pageResult.getPageSize();
        int start = ((pageResult.getPageNum() - 1 )* size);
        List<User> userList = userMapper.findByPage(start,size);
        pageResult.setRows(userList).setToatal(total);
        return pageResult;*/
        //1,定义分页对象
        boolean flag = StringUtils.hasLength(pageResult.getQuery());
        IPage<User> page = new Page<>(pageResult.getPageNum(),pageResult.getPageSize());
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.like(flag, "username", pageResult.getQuery());
        page = userMapper.selectPage(page, queryWrapper);
        pageResult.setTotal(page.getTotal()).setRows(page.getRecords());
        return pageResult;
    }

    @Override
    public void updateStatus(User user) {
        userMapper.updateById(user);
    }

    @Override
    public void addUser(User user) {
        String password = user.getPassword();
        password = DigestUtils.md5DigestAsHex(password.getBytes());
        user.setPassword(password)
                .setStatus(true);
//                .setCreated(new Date())
//                .setUpdated(user.getCreated());
        userMapper.insert(user);


    }

    @Override
    @Transactional(noRollbackFor = {RuntimeException.class})
    public void deleteUser(Integer id) {
        userMapper.deleteById(id);
    }

    @Override
    public User findById(Integer id) {
        return userMapper.selectById(id);
    }

    @Override
    public void updateUser(User user) {
        userMapper.updateById(user);
    }
}
