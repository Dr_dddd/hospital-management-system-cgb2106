package com.cy.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@TableName("potion")
public class Potion extends BasePojo{
    @TableId(type = IdType.AUTO)
    private Integer potionId;
    private String potionNo;
    private String potionName;
    private Integer potionPrice;
    private Integer potionQuantity;
    private String potionRen;
    private Boolean potionStatus;

}
