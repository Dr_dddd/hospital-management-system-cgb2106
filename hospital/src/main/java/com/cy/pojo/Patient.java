package com.cy.pojo;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@TableName("patient")
public class Patient extends BasePojo{
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String patientName;
    private String sex;
    private String state;//症状
    private String bedId;
    private String roomId;
    private Boolean status;//状态
}
