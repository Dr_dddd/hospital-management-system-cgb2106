package com.cy.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cy.pojo.BasePojo;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author 刘昱江
 * 时间 2021/4/7
 */
@TableName("room")
@Data
@Accessors(chain = true)
public class Room extends BasePojo {
    @TableId(type = IdType.AUTO,value = "room_id")
    private Integer roomId;       //房间ID
    @TableField("room_no")
    private String roomNo;          //房间编号
    @TableField("room_type")
    private Integer roomType;      //房间等级
    @TableField("room_grade")
    private String roomGrade;        //房间类型
    @TableField("bed_used")
    private Integer bedUsed;      //床位占用数
    @TableField("room_bedempty")
    private Integer roomBedempty;   //空床数目
    @TableField("room_judge")
    private Boolean roomJudge;       //房间状态
}