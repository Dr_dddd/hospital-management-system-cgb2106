package com.cy.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
@TableName("dept")
public class Dept implements Serializable {
    @TableId(type = IdType.AUTO)
    private Integer deptId;
    private String deptNo;
    private String deptName;
    private String deptManager;
    private String deptViceManager;
}
