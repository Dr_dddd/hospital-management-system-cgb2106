package com.cy.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@Data
@Accessors(chain = true)
@TableName("personnel")
public class Personnel implements Serializable {
    @TableId(type = IdType.AUTO)
    private Integer empId;
    private String empNo;
    private String empName;
    private String empDeptld;
    private String empDuty;
    private String empXl;
    private String empGender;
    private String empHometown;
    private Boolean empHealth;

    private Date empStartWork;
    private String empEmail;
}
