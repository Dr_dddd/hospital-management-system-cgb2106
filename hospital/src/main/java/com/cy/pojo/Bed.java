package com.cy.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@TableName("bed")
@Data
@Accessors(chain = true)
public class Bed extends BasePojo implements Serializable {
    @TableId(type = IdType.AUTO)
    private Integer bedId;         //病床ID
    private String bedRoomId;      //房间编号
    private String bedNo;         //病床编号
    @TableField(value = "bed_patient_name")
    private String bedPatientName;       //病人名字
    private Boolean bedJudge;       //病床状态

}