package com.cy.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

@TableName("appartus")
@Data
@Accessors(chain = true)
public class Appartus {
    @TableId(type = IdType.AUTO)
    private Integer appartusId;
    private String appartusName;
    private Integer appartusPrice;
    private Integer appartusNumber;
    private  String appartusProductiondate;
    private  String appartusProductionaddress;
    private  String appartusClassify;
}
