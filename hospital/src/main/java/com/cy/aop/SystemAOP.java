package com.cy.aop;

import com.cy.vo.SysResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 注解的作用:
 *  1.该注解只拦截Controller层抛出的异常信息
 * */
@RestControllerAdvice
public class SystemAOP {
    @ExceptionHandler(RuntimeException.class)
    public SysResult exception(Exception e){
        e.printStackTrace();
        return SysResult.fail();
    }
}
