/*
SQLyog 企业版 - MySQL GUI v8.14 
MySQL - 5.5.5-10.3.7-MariaDB 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

create table `room` (
	`room_id` double ,
	`room_no` varchar (30),
	`room_type` double ,
	`room_grade` varchar (60),
	`bed_used` double ,
	`room_bedempty` double ,
	`room_judge` tinyint (1),
	`created` datetime ,
	`updated` datetime 
); 
insert into `room` (`room_id`, `room_no`, `room_type`, `room_grade`, `bed_used`, `room_bedempty`, `room_judge`, `created`, `updated`) values('1','fj101','3','六人间','4','2','0','2021-10-12 11:52:18','2021-10-12 16:18:22');
insert into `room` (`room_id`, `room_no`, `room_type`, `room_grade`, `bed_used`, `room_bedempty`, `room_judge`, `created`, `updated`) values('2','fj102','3','六人间','0','6','0','2021-10-12 11:58:53','2021-10-12 16:18:15');
insert into `room` (`room_id`, `room_no`, `room_type`, `room_grade`, `bed_used`, `room_bedempty`, `room_judge`, `created`, `updated`) values('4','fj103','3','六人间','0','6','0','2021-10-12 11:59:25','2021-10-12 14:15:45');
insert into `room` (`room_id`, `room_no`, `room_type`, `room_grade`, `bed_used`, `room_bedempty`, `room_judge`, `created`, `updated`) values('5','fj201','2','四人间','0','4','0','2021-10-12 13:51:37','2021-10-12 13:56:01');
