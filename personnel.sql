/*
SQLyog 企业版 - MySQL GUI v8.14 
MySQL - 5.5.5-10.3.7-MariaDB 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

create table `personnel` (
	`emp_id` double ,
	`emp_no` varchar (300),
	`emp_name` varchar (300),
	`emp_deptld` varchar (300),
	`emp_duty` varchar (300),
	`emp_xl` varchar (300),
	`emp_gender` varchar (300),
	`emp_hometown` varchar (300),
	`emp_health` tinyint (1),
	`emp_start_work` date ,
	`emp_email` varchar (300)
); 
insert into `personnel` (`emp_id`, `emp_no`, `emp_name`, `emp_deptld`, `emp_duty`, `emp_xl`, `emp_gender`, `emp_hometown`, `emp_health`, `emp_start_work`, `emp_email`) values('1','yg001','李斌','bm001','主任','本科','男','北京市','1','2021-06-15','1111111111@qq.com');
insert into `personnel` (`emp_id`, `emp_no`, `emp_name`, `emp_deptld`, `emp_duty`, `emp_xl`, `emp_gender`, `emp_hometown`, `emp_health`, `emp_start_work`, `emp_email`) values('2','yg002','王斌','bm001','副主任','研究生','男','北京市','1','2021-06-15','1111111111@qq.com');
insert into `personnel` (`emp_id`, `emp_no`, `emp_name`, `emp_deptld`, `emp_duty`, `emp_xl`, `emp_gender`, `emp_hometown`, `emp_health`, `emp_start_work`, `emp_email`) values('3','yg003','李文斌','bm002','主任','本科','男','北京市','1','2021-06-16','1111111111@qq.com');
insert into `personnel` (`emp_id`, `emp_no`, `emp_name`, `emp_deptld`, `emp_duty`, `emp_xl`, `emp_gender`, `emp_hometown`, `emp_health`, `emp_start_work`, `emp_email`) values('4','yg004','吴克','bm002','副主任','本科','男','北京市','1','2021-06-20','1111111111@qq.com');
insert into `personnel` (`emp_id`, `emp_no`, `emp_name`, `emp_deptld`, `emp_duty`, `emp_xl`, `emp_gender`, `emp_hometown`, `emp_health`, `emp_start_work`, `emp_email`) values('5','yg005','邓文','bm003','主任','本科','男','北京市','1','2021-06-20','1111111111@qq.com');
insert into `personnel` (`emp_id`, `emp_no`, `emp_name`, `emp_deptld`, `emp_duty`, `emp_xl`, `emp_gender`, `emp_hometown`, `emp_health`, `emp_start_work`, `emp_email`) values('6','yg006','戴紫莹','bm003','副主任','本科','女','北京市','1','2021-06-21','1111111111@qq.com');
insert into `personnel` (`emp_id`, `emp_no`, `emp_name`, `emp_deptld`, `emp_duty`, `emp_xl`, `emp_gender`, `emp_hometown`, `emp_health`, `emp_start_work`, `emp_email`) values('7','yg007','吴秋实','bm004','主任','本科','男','北京市','1','2021-06-21','1111111111@qq.com');
insert into `personnel` (`emp_id`, `emp_no`, `emp_name`, `emp_deptld`, `emp_duty`, `emp_xl`, `emp_gender`, `emp_hometown`, `emp_health`, `emp_start_work`, `emp_email`) values('8','yg008','欧阳','bm004','副主任','本科','女','北京市','1','2021-06-22','1111111111@qq.com');
insert into `personnel` (`emp_id`, `emp_no`, `emp_name`, `emp_deptld`, `emp_duty`, `emp_xl`, `emp_gender`, `emp_hometown`, `emp_health`, `emp_start_work`, `emp_email`) values('9','yg009','刘浩存','bm001','护士','本科','男','北京市','1','2021-06-22','1111111111@qq.com');
insert into `personnel` (`emp_id`, `emp_no`, `emp_name`, `emp_deptld`, `emp_duty`, `emp_xl`, `emp_gender`, `emp_hometown`, `emp_health`, `emp_start_work`, `emp_email`) values('10','yg00110','曹颖','bm002','医师','本科','女','北京市','1','2021-06-19','1111111111@qq.com');
insert into `personnel` (`emp_id`, `emp_no`, `emp_name`, `emp_deptld`, `emp_duty`, `emp_xl`, `emp_gender`, `emp_hometown`, `emp_health`, `emp_start_work`, `emp_email`) values('11','yg0011','张晓晓','bm003','护士','本科','女','北京市','1','2021-06-19','1111111111@qq.com');
insert into `personnel` (`emp_id`, `emp_no`, `emp_name`, `emp_deptld`, `emp_duty`, `emp_xl`, `emp_gender`, `emp_hometown`, `emp_health`, `emp_start_work`, `emp_email`) values('13','yg0013','蒋书评','bm002',' 院长','本科','男','北京市','1','2021-06-18','2365413635@qq.com');
