/*
SQLyog 企业版 - MySQL GUI v8.14 
MySQL - 5.5.5-10.3.7-MariaDB 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

create table `bed` (
	`bed_id` double ,
	`bed_room_id` varchar (30),
	`bed_no` varchar (30),
	`bed_patient_name` varchar (90),
	`bed_judge` tinyint (1),
	`created` datetime ,
	`updated` datetime 
); 
insert into `bed` (`bed_id`, `bed_room_id`, `bed_no`, `bed_patient_name`, `bed_judge`, `created`, `updated`) values('1','fj101','bc001','','0','2021-10-12 11:52:18','2021-10-12 16:17:35');
insert into `bed` (`bed_id`, `bed_room_id`, `bed_no`, `bed_patient_name`, `bed_judge`, `created`, `updated`) values('2','fj101','bc002','101-2','1','2021-10-12 11:52:18','2021-10-12 13:54:22');
insert into `bed` (`bed_id`, `bed_room_id`, `bed_no`, `bed_patient_name`, `bed_judge`, `created`, `updated`) values('3','fj101','bc003','101-3','1','2021-10-12 11:52:18','2021-10-12 13:54:39');
insert into `bed` (`bed_id`, `bed_room_id`, `bed_no`, `bed_patient_name`, `bed_judge`, `created`, `updated`) values('4','fj101','bc004','101-4','1','2021-10-12 11:52:18','2021-10-12 13:54:52');
insert into `bed` (`bed_id`, `bed_room_id`, `bed_no`, `bed_patient_name`, `bed_judge`, `created`, `updated`) values('5','fj101','bc005','101-5','1','2021-10-12 11:52:18','2021-10-12 16:18:22');
insert into `bed` (`bed_id`, `bed_room_id`, `bed_no`, `bed_patient_name`, `bed_judge`, `created`, `updated`) values('6','fj101','bc006','101-6','1','2021-10-12 11:52:18','2021-10-12 16:12:47');
insert into `bed` (`bed_id`, `bed_room_id`, `bed_no`, `bed_patient_name`, `bed_judge`, `created`, `updated`) values('7','fj102','bc001','','0','2021-10-12 11:58:54','2021-10-12 16:12:47');
insert into `bed` (`bed_id`, `bed_room_id`, `bed_no`, `bed_patient_name`, `bed_judge`, `created`, `updated`) values('8','fj102','bc002','','0','2021-10-12 11:58:54','2021-10-12 14:39:58');
insert into `bed` (`bed_id`, `bed_room_id`, `bed_no`, `bed_patient_name`, `bed_judge`, `created`, `updated`) values('11','fj103','bc001',NULL,'0','2021-10-12 11:59:25','2021-10-12 13:55:56');
insert into `bed` (`bed_id`, `bed_room_id`, `bed_no`, `bed_patient_name`, `bed_judge`, `created`, `updated`) values('12','fj103','bc002',NULL,'0','2021-10-12 11:59:25','2021-10-12 13:55:57');
insert into `bed` (`bed_id`, `bed_room_id`, `bed_no`, `bed_patient_name`, `bed_judge`, `created`, `updated`) values('26','fj103','bc003',NULL,'0','2021-10-12 13:29:21','2021-10-12 13:55:57');
insert into `bed` (`bed_id`, `bed_room_id`, `bed_no`, `bed_patient_name`, `bed_judge`, `created`, `updated`) values('27','fj103','bc004',NULL,'0','2021-10-12 13:29:21','2021-10-12 13:55:58');
insert into `bed` (`bed_id`, `bed_room_id`, `bed_no`, `bed_patient_name`, `bed_judge`, `created`, `updated`) values('28','fj103','bc005','','0','2021-10-12 13:29:21','2021-10-12 14:00:43');
insert into `bed` (`bed_id`, `bed_room_id`, `bed_no`, `bed_patient_name`, `bed_judge`, `created`, `updated`) values('29','fj103','bc006','','0','2021-10-12 13:29:21','2021-10-12 14:15:45');
insert into `bed` (`bed_id`, `bed_room_id`, `bed_no`, `bed_patient_name`, `bed_judge`, `created`, `updated`) values('34','fj201','bc001',NULL,'0','2021-10-12 13:51:37','2021-10-12 13:56:00');
insert into `bed` (`bed_id`, `bed_room_id`, `bed_no`, `bed_patient_name`, `bed_judge`, `created`, `updated`) values('35','fj201','bc002',NULL,'0','2021-10-12 13:51:37','2021-10-12 13:56:01');
insert into `bed` (`bed_id`, `bed_room_id`, `bed_no`, `bed_patient_name`, `bed_judge`, `created`, `updated`) values('36','fj201','bc003',NULL,'0','2021-10-12 13:51:37','2021-10-12 13:51:37');
insert into `bed` (`bed_id`, `bed_room_id`, `bed_no`, `bed_patient_name`, `bed_judge`, `created`, `updated`) values('37','fj201','bc004',NULL,'0','2021-10-12 13:51:37','2021-10-12 13:51:37');
insert into `bed` (`bed_id`, `bed_room_id`, `bed_no`, `bed_patient_name`, `bed_judge`, `created`, `updated`) values('40','fj102','bc003',NULL,'0','2021-10-12 16:18:15','2021-10-12 16:18:15');
insert into `bed` (`bed_id`, `bed_room_id`, `bed_no`, `bed_patient_name`, `bed_judge`, `created`, `updated`) values('41','fj102','bc004',NULL,'0','2021-10-12 16:18:15','2021-10-12 16:18:15');
insert into `bed` (`bed_id`, `bed_room_id`, `bed_no`, `bed_patient_name`, `bed_judge`, `created`, `updated`) values('42','fj102','bc005',NULL,'0','2021-10-12 16:18:15','2021-10-12 16:18:15');
insert into `bed` (`bed_id`, `bed_room_id`, `bed_no`, `bed_patient_name`, `bed_judge`, `created`, `updated`) values('43','fj102','bc006',NULL,'0','2021-10-12 16:18:15','2021-10-12 16:18:15');
